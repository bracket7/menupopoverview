//
//  ViewController.m
//  MenuPopOverView
//
//  Created by Camel Yang on 4/4/14.
//  Copyright (c) 2014 camelcc. All rights reserved.
//

#import "ViewController.h"

#import "MenuPopOverView.h"

@interface ViewController () <MenuPopOverViewDelegate>

@property (weak, nonatomic) MenuPopOverView *popOver;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    [self.view addGestureRecognizer:tap];
}

- (void)tapped:(UITapGestureRecognizer *)tap
{
    UIColor *redColor = [UIColor colorWithRed:203/255.0 green:82/255.0 blue:13/255.0 alpha:1];
    UIColor *lightRedColor = [UIColor colorWithRed:245/255.0 green:221/255.0 blue:207/255.0 alpha:1];

    [self.popOver dismiss:YES];

    CGPoint point = [tap locationInView:self.view];
    MenuPopOverView *popOver = [[MenuPopOverView alloc] init];
    popOver.delegate = self;
    popOver.popOverBackgroundColor = [UIColor whiteColor];
    popOver.popOverHighlightedColor = lightRedColor;
    popOver.popOverSelectedColor = redColor;
    popOver.popOverTextColor = redColor;
    popOver.popOverDividerColor = redColor;
    popOver.popOverBorderColor = redColor;
    popOver.popOverHighlightedTextColor = redColor;
    popOver.popOverSelectedTextColor = [UIColor whiteColor];
    popOver.arrowDirectionOffset = -6.5;

//    [popOver presentPopoverFromRect:CGRectMake(point.x, point.y, 0, 0) inView:self.view withStrings:@[@"Less", @"Normal", @"More"]];
    [popOver presentPopoverFromRect:CGRectMake(point.x, point.y, 0, 0) inView:self.view withStrings:@[@"Less", @"Normal", @"More"] selectedIndex:1];
//    [popOver presentPopoverFromRect:CGRectMake(point.x, point.y, 0, 0) inView:self.view withStrings:@[@"1", @"22222", @"333333333"] selectedIndex:2];
//    [popOver presentPopoverFromRect:CGRectMake(point.x, point.y, 0, 0) inView:self.view withStrings:@[@"Mild", @"Medium", @"Hot"] selectedIndex:1];
//    [popOver presentPopoverFromRect:CGRectMake(point.x, point.y, 0, 0) inView:self.view withStrings:@[@"Less", @"Normal", @"More", @"Less", @"Normal", @"More"] selectedIndex:1];
//    [popOver presentPopoverFromRect:CGRectMake(point.x, point.y, 0, 0) inView:self.view withStrings:@[@"Normal"]];
    self.popOver = popOver;
}

- (void)popoverViewDidExpand:(MenuPopOverView *)popoverView {
    NSLog(@"popOver expanded.");

    CGPoint center = popoverView.center;
    center.x -= 50;
    [UIView animateWithDuration:0.3 animations:^{
        popoverView.center = center;
    }];
}

- (void)popoverView:(MenuPopOverView *)popoverView didSelectItemAtIndex:(NSInteger)index {
    NSLog(@"popOver selected item %ld", (long)index);

    CGPoint center = popoverView.center;
    center.x += 50;
    [UIView animateWithDuration:0.3 animations:^{
        popoverView.center = center;
    }];
}

- (void)popoverViewDidDismiss:(MenuPopOverView *)popoverView {
    NSLog(@"popOver dismissed.");
}

@end
